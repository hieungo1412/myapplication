package com.dut.hieungo1412.myapplication;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ListView;
import android.widget.RadioButton;
import android.widget.RadioGroup;

import java.util.ArrayList;
import java.util.List;

public class MainActivity extends AppCompatActivity {

    int checkSex;
    Button btnAdd;
    EditText edtId;
    EditText edtName;
    RadioGroup rdG;
    RadioButton rdFemale;
    RadioButton rdMale;
    ImageButton btnDelete;
    CheckBox ckb;
    ListView lvStd;
    ArrayList<Student> al = null;
    MyArrayAdapter adapter = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        init();

        al = new ArrayList<>();
        adapter = new MyArrayAdapter(this, R.layout.custom_layout, al);
        lvStd.setAdapter(adapter);
        rdG.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                checkSex = checkedId;
            }
        });
        btnAdd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                add();
                adapter.notifyDataSetChanged();
            }
        });

        btnDelete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                delete();
                adapter.notifyDataSetChanged();
            }
        });
    }

    public void init() {
        btnAdd = (Button) findViewById(R.id.btnAdd);
        btnDelete = (ImageButton) findViewById(R.id.btnDelete);
        edtId = (EditText) findViewById(R.id.editId);
        edtName = (EditText) findViewById(R.id.editName);
        rdG = (RadioGroup) findViewById(R.id.radioGroup1);
        rdFemale = (RadioButton) findViewById(R.id.radFemale);
        rdMale = (RadioButton) findViewById(R.id.radMale);
        ckb = (CheckBox) findViewById(R.id.chkItem);
        lvStd = (ListView) findViewById(R.id.lvStd);
    }

    public void add() {
        int id = Integer.parseInt(edtId.getText().toString());
        String name = edtName.getText().toString();
        int sex;
        if (checkSex == R.id.radFemale) {
            sex = 1;
        } else {
            sex = 0;
        }
        Student std = new Student(id, name, sex);
        al.add(std);
    }

    public void delete() {
        for (int i = lvStd.getChildCount(); i >= 0; i--) {
            View v = lvStd.getChildAt(i);
            if (ckb.isChecked()) {
                al.remove(i);
            }
        }
    }


}
