package com.dut.hieungo1412.myapplication;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Adapter;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by HIEUNGO1412 on 02/06/2017.
 */

public class MyArrayAdapter extends ArrayAdapter<Student> {

    Activity context;
    int layoutId;
    ArrayList<Student> arr;

    public MyArrayAdapter(Activity context, int layoutId, ArrayList<Student> arr) {
        super(context, layoutId, arr);
        this.context = context;
        this.layoutId = layoutId;
        this.arr = arr;
    }

    public View getView(int position, View convertView, ViewGroup parent) {
        LayoutInflater inflater = context.getLayoutInflater();
        convertView = inflater.inflate(layoutId, null);

        final TextView txtDisplay = (TextView) convertView.findViewById(R.id.txtItem);
        final Student std = arr.get(position);
        txtDisplay.setText(std.toString());
        final ImageView imgItem = (ImageView) convertView.findViewById(R.id.imgItem);
        if (std.getSex() == 1) {
            imgItem.setImageResource(R.drawable.girl);
        } else {
            imgItem.setImageResource(R.drawable.boy);
        }

        return convertView;
    }
}
